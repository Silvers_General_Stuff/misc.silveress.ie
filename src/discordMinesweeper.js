import React, { Component } from 'react';

class DiscordMinesweeper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultSize:5,
            defaultDifficulty:3,
            copySuccess:""
        }
        this.handleSizeSelection = this.handleSizeSelection.bind(this);
        this.handleDifficultySelection = this.handleDifficultySelection.bind(this);
    }

    handleSizeSelection(event){
        let tmp = {};
        tmp.defaultSize = event.target.value;
        this.setState(tmp)
    }

    handleDifficultySelection(event){
        let tmp = {};
        tmp.defaultDifficulty = event.target.value;
        this.setState(tmp)
    }

    copyToClipboard = (e) => {
        this.textArea.select();
        document.execCommand('copy');
        // This is just personal preference.
        // I prefer to not show the the whole text area selected.
        e.target.focus();
    }

    static createMine(difficulty) {
        let chance = Math.random();
        difficulty = difficulty -0
        let result = ":boom:"
        switch (difficulty) {
            case 0:if (chance > 0.00) {result = ""};break;
            case 1:if (chance > 0.05) {result = ""};break;
            case 2:if (chance > 0.10) {result = ""};break;
            case 3:if (chance > 0.15) {result = ""};break;
            case 4:if (chance > 0.20) {result = ""};break;
            case 5:if (chance > 0.25) {result = ""};break;
            case 6:if (chance > 0.30) {result = ""};break;
            case 7:if (chance > 0.35) {result = ""};break;
            case 8:if (chance > 0.40) {result = ""};break;
            case 9:if (chance > 0.45) {result = ""};break;
            case 10:result = ":boom:";break;
            default: result = ""
        }
        return result
    }

    static numberToEmoji(number){
        let result
        switch (number) {
            case 0:result = ":zero:";break;
            case 1:result = ":one:";break;
            case 2:result = ":two:";break;
            case 3:result = ":three:";break;
            case 4:result = ":four:";break;
            case 5:result = ":five:";break;
            case 6:result = ":six:";break;
            case 7:result = ":seven:";break;
            case 8:result = ":eight:";break;
            case 9:result = ":nine:";break;
            default:result = ":zero:";
        }
        return result
    }

    static generateTable(size, difficulty){
        let tmp = {}
        for(let i=0;i<size;i++){
            tmp[i] = {}
            for(let j=0;j<size;j++){
                tmp[i][j] = DiscordMinesweeper.createMine(difficulty)
            }
        }
        let outputText = ""
        outputText += "Silveress's Discord Minesweeper.\n"
        outputText += "https://misc.silveress.ie/discord_minesweeper \n"
        outputText += "Size: "+ size +"x"+ size +"\n"
        outputText += "Difficulty: "+ difficulty +"/10\n"
        outputText += "Switch channel to reset.\n"

        for(let i=0;i<size;i++){
            let row = []
            for(let j=0;j<size;j++){
                if(tmp[i][j] !== ":boom:"){
                    let flag = 0;
                    for(let k=-1;k<2;k++){
                        if(typeof tmp[i+k] !== "undefined"){
                            for(let l=-1;l<2;l++){
                                if(typeof tmp[i+k][j+l] !== "undefined"){
                                    if(tmp[i+k][j+l] === ":boom:"){
                                        flag++
                                    }
                                }
                            }

                        }
                    }
                    tmp[i][j] = DiscordMinesweeper.numberToEmoji(flag)
                }
                row.push(tmp[i][j])
            }
            outputText += "||"+ row.join("||||") +"||\n"
        }
        return outputText
    }

    render() {
        let sizeDropdown = [];
        for(let i=1;i<14;i++){
            sizeDropdown.push(
              <option key={i} value={i}>{i +"x"+i}</option>
            )
        }
        let characterSelect = <select
          value={this.state.defaultSize}
          onChange={this.handleSizeSelection}>
            {sizeDropdown}
        </select>

        let difficultyDropdown = [];
        for(let i=1;i<11;i++){
            difficultyDropdown.push(
              <option key={i} value={i}>{i +"/10"}</option>
            )
        }
        let difficultySelect = <select
          value={this.state.defaultDifficulty}
          onChange={this.handleDifficultySelection}>
            {difficultyDropdown}
        </select>
        let value = DiscordMinesweeper.generateTable(this.state.defaultSize,this.state.defaultDifficulty)
        return <div style={{align:"center"}} >
            <h3>Discord Minesweeper Creator</h3>
            <h4>Uses spoiler tags to create a randomised minesweeper for Discord</h4>
            <h5>Because all the best ideas happen at 2am with a litre of coffee...</h5>
            <span>Size: {characterSelect}&nbsp;&nbsp; Difficulty: {difficultySelect}</span>
            <br />
            <br />

            <span>Click/Tap text below to copy it to clipboard. Then paste into Discord.</span>
            <form>
                <textarea
                  readOnly
                  onClick={this.copyToClipboard}
                  ref={(textarea) => this.textArea = textarea}
                  value={value}
                />
            </form>
        </div>
    }
}

export default DiscordMinesweeper;
