import React from "react";
import { Route, Switch } from "react-router-dom";

import withTracker from "./withTracker";

import Home from "./home";
import discordMinesweeper from "./discordMinesweeper";

export default () =>
  <Switch>
          <Route exact path="/" component={withTracker(Home)} />
          <Route path="/discord_minesweeper" component={withTracker(discordMinesweeper)} />
  </Switch>;