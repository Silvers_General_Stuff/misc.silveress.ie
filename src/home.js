import React, { Component } from 'react';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return <div style={{align:"center"}} >
            <h3>Welcome!</h3>
            <p>
                This is where I put my random stuff.
                <br />
                Source code is available on <a href={"https://gitlab.com/Silvers_General_Stuff/misc.silveress.ie"} target={"_blank"}>Gitlab</a>
            </p>

        </div>;
    }
}

export default Home;
