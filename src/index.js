import './index.css';
import './App.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link} from "react-router-dom";
import { Nav, Navbar, NavDropdown, NavItem, } from 'react-bootstrap'
import { LinkContainer } from "react-router-bootstrap";
import Routes from "./routes";
import { version } from '../package.json'

const MainNavbar = () => (
  <Router>
    <div className='App container'>
      <Navbar fluid collapseOnSelect >
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/'>Home <sub>v{version}</sub></Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <NavDropdown title='Discord' id='basic-nav-dropdown'>
              <LinkContainer to='/discord_minesweeper'>
                <NavItem>Minesweeper</NavItem>
              </LinkContainer>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Routes />
    </div>
  </Router>)

ReactDOM.render(<MainNavbar />, document.getElementById('root'));